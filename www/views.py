from django.db import transaction
from django.shortcuts import render
from www import forms, models


# Create your views here.
def index(request):
    return render(request, 'index.html')


def extras(request):
    return render(request, 'extras.html')


def register(request):
    return render(request, 'register.html')


def schedules(request):
    context = {'events': models.Event.objects.all()}
    if request.method == 'POST':
        form = forms.EventForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                form.save()
    else:
        form = forms.EventForm()
    context['form'] = form
    return render(request, 'schedules.html', context)
