from django.db import models


# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=80, help_text='Insert name')
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=80, help_text='Insert location')
    category = models.CharField(max_length=80, help_text='Insert category')
