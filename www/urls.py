from django.urls import re_path
from django.shortcuts import render
from www import views

urlpatterns = [
        re_path(r'^heretic$', lambda req: render(req, 'heretic.html')),
        re_path(r'^register$', views.register, name='register'),
        re_path(r'^extras$', views.extras, name='extras'),
        re_path(r'^schedules$', views.schedules, name='schedules'),
        re_path(r'^$', views.index, name='index'),
    ]
