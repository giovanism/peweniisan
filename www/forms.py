from django import forms
from www import models


class DateInput(forms.DateInput):
    input_type = 'date'


class TimeInput(forms.TimeInput):
    input_type = 'time'

class EventForm(forms.ModelForm):
    class Meta:
        model = models.Event
        fields = ['name', 'date', 'time', 'location', 'category']
        widgets = {
                'date': DateInput(),
                'time': TimeInput(),
            }
